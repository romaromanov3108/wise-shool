$(document).ready(function() {
    function ajax(data, url)
    {
        $.ajax({
            url: url,
            type: 'post',
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
                console.log(data);
                if(data == 'create'){
                    mesage('Замовлення успішно створено!');
                    setTimeout(function(){
                        window.location.replace("/");
                    },3500);
                }
                else if(data == 'read') {
                    mesage('Зміни успішно збережено!');
                }
            },
            error: function () {
                $('.stop').html('УПС щось пішло не так!');
                $('.stop').show();
                setTimeout(function () {
                    $(".stop").fadeOut("slow")
                }, 3000);
            }
        });
    }
    //Alert message
    function mesage(text)
    {
        $('.ok').html(text);
        $('.ok').show();
        setTimeout(function () {
            $(".ok").fadeOut("slow")
        }, 4000);
    }
    //alert error
    function stop()
    {
        $('.stop').html('Заповніть обов`язкові поля!');
        $('.stop').show();
        setTimeout(function () {
            $(".stop").fadeOut("slow")
        }, 4000);
    }
    //required input valid
    function valid(data)
    {
        var eror = 0;

        if(data.get('name') == ''){
            eror++;
            $('[name=name]').css('border', '1px solid red');
        }
        if(data.get('phone') == ''){
            eror++;
            $('[name=phone]').css('border', '1px solid red');
        }
        if(eror > 0){
            return 'stop';
        }

    }
    //click create, send form create order
    $('#btn-create').click(function (e) {
        e.preventDefault();
        var forma = $('#cretae-order');
        var data = new FormData(forma.get(0));
        var url = '/create-order';

        if(valid(data) == 'stop'){
            stop();
            return false;
        }else{
            ajax(data,url);
        }

    });

    $('#btn-read').click(function(e){
        e.preventDefault();
        var forma = $('#read-order');
        var data = new FormData(forma.get(0));
        var url = '/read-order/saved';

        if(valid(data) == 'stop'){
            stop();
            return false;
        }else{
            ajax(data,url);
        }
    });

    $('[href ^="/order-deleted/"]').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');

        $.get(url, function(data){
            $('[href ^="/order-deleted/'+data+'"]').parent().parent().empty();

            mesage('Замовлення видалено!')

        });
    });

});