<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;

class PostController extends Controller
{
    public function doCreate(Request $request)
    {
        $orders = new Orders();

        $orders->name = $request->name;
        $orders->phone = $request->phone;
        $orders->email = $request->email;
        $orders->comment = $request->comment;
        $orders->save();

        return 'create';
    }

    public function doRead(Request $request)
    {
        Orders::where('id', $request->id)->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'comment' => $request->comment
        ]);

        return 'read';
    }
}
