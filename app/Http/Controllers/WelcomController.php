<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;

class WelcomController extends Controller
{
    public function doWelcom ()
    {
        $orders = Orders::all();
        $data = [
            'orders' => $orders
        ];

        return view('element.welcom', $data);
    }

    public function doCreateOrder()
    {
        return view('element.form.create');
    }

    public function doReadOrder($id)
    {
        $order = Orders::where('id', $id)->get();
        $data = [
            'orders' => $order
        ];

        return view('element.form.read', $data);
    }
    public function doDeletOrder($id)
    {
        Orders::where('id', $id)->delete();

        return $id;
    }
}
