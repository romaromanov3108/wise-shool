<?php

use Illuminate\Database\Seeder;
use App\Orders;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Роман',
                'phone' => '0672603284',
                'email' => 'romanych914@gmail.com',
                'comment' => 'Тестовий заказ'
            ],
            [
                'name' => 'Іван',
                'phone' => '0672623425',
                'email' => 'test@gmail.com',
                'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam '
            ]
        ];
        Orders::insert($data);
    }
}
