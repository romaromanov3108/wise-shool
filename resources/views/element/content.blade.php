@extends('index')
@section('content')
@include('element.top-menu')
@include('element.alert')

<div class="container">
    <div class="row">
        <div class="col-lg">
            @yield('element')
        </div>
    </div>
</div>


@endsection