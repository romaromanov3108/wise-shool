@extends('element.content')
@section('element')
    <h4 style="text-align: center;">Створити Замовлення</h4>
    <div class="col-lg-4 col-lg-offset-4">
        <form id="cretae-order">
            @csrf
            <div class="form-group">
                <label>Ім'я</label>
                <input type="text" name="name" class="form-control form-control-sm" placeholder="Kot">
            </div>
            <div class="form-group">
                <label>Номер телефону</label>
                <input type="text" name="phone" class="form-control form-control-sm" placeholder="+380">
            </div>
            <div class="form-group">
                <label>email</label>
                <input type="text" name="email" class="form-control form-control-sm" placeholder="koteyka@gmail.com">
            </div>
            <div class="form-group">
                <label>Коментар</label>
                <textarea class="form-control form-control-sm" name="comment" placeholder="ляляляляллял"></textarea>
            </div>
        </form>
        <button class="btn btn-success btn-sm" id="btn-create"><i class="glyphicon glyphicon-ok"></i> Створити</button>
    </div>

@endsection
