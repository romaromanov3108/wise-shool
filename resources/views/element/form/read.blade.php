@extends('element.content')
@section('element')
    <h4 style="text-align: center;">Створити Замовлення</h4>
    <div class="col-lg-4 col-lg-offset-4">
        <form id="read-order">
            @csrf
            @foreach($orders as $order)
            <div class="form-group">
                <label>Ім'я</label>
                <input type="text" name="name" class="form-control form-control-sm" value="{{$order->name}}" placeholder="Kot">
            </div>
            <div class="form-group">
                <label>Номер телефону</label>
                <input type="text" name="phone" class="form-control form-control-sm" value="{{$order->phone}}" placeholder="+380">
            </div>
            <div class="form-group">
                <label>email</label>
                <input type="text" name="email" class="form-control form-control-sm" value="{{$order->email}}" placeholder="koteyka@gmail.com">
            </div>
            <div class="form-group">
                <label>Коментар</label>
                <input type="hidden" name="id" value="{{$order->id}}">
                <textarea name="comment" class="form-control form-control-sm" placeholder="ляляляляллял">{{$order->comment}}</textarea>
            </div>
            @endforeach
        </form>
        <button class="btn btn-success btn-sm" id="btn-read"><i class="glyphicon glyphicon-floppy-saved"></i> Зберегти</button>
    </div>

@endsection
