@extends('element.content')
@section('element')
    <h4>Список Замовлень</h4>
    <a href="/create-order" class="btn btn-success btn-sm" style="float: right;"><i class="glyphicon glyphicon-plus"></i> Додати замовлення</a>
    <table class="table table-sm">
        <thead>
        <th scope="col">ID</th>
        <th scope="col">Ім'я</th>
        <th scope="col">Номер</th>
        <th scope="col">email</th>
        <th scope="col">коментар</th>
        <th>Дата створення</th>
        <th>дії</th>
        </thead>
        <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->phone}}</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->comment}}</td>
                    <td>{{$order->created_at}}</td>
                    <td>
                        <a href="/read-order/{{$order->id}}" class="btn btn-info btn-sm">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        <a href="/order-deleted/{{$order->id}}" class="btn btn-danger btn-sm">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection